package sda.java32.service.mapper;

import org.springframework.stereotype.Component;
import sda.java32.dto.UserCreateDTO;
import sda.java32.dto.UserDTO;
import sda.java32.entity.AppUser;

@Component
public class UserMapper {

    public AppUser toEntity(UserCreateDTO createDTO) {
        return AppUser.builder()
                .userName(createDTO.getUserName())
                .password(createDTO.getPassword())
                .build();
    }

    // we ignore password on purpose as to no have security leaks
    public UserDTO toDTO(AppUser entity) {
        return  UserDTO.builder()
                .id(entity.getId())
                .userName(entity.getUserName())
                .build();
    }
}
