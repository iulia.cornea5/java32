package sda.java32.service.mapper;

import org.springframework.stereotype.Component;
import sda.java32.dto.ProductCreateDTO;
import sda.java32.dto.ProductDTO;
import sda.java32.entity.Product;

@Component
public class ProductMapper {

    public Product mapToEntity(ProductCreateDTO createDTO){
        return Product.builder()
                .name(createDTO.getName())
                .description(createDTO.getDescription())
                .acquisitionPrice(createDTO.getAcquisitionPrice())
                .sellingPrice(createDTO.getSellingPrice())
                .build();
    }

    public ProductDTO toDto(Product entity) {
        return  ProductDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .description(entity.getDescription())
                .acquisitionPrice(entity.getAcquisitionPrice())
                .sellingPrice(entity.getSellingPrice())
                .build();
    }

}
