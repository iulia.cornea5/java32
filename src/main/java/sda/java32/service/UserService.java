package sda.java32.service;

import org.springframework.http.ResponseEntity;
import sda.java32.dto.UserCreateDTO;
import sda.java32.dto.UserDTO;

import java.util.List;

public interface UserService {

    UserDTO create(UserCreateDTO createDTO);

    List<UserDTO> getAll();

    UserDTO getById(Long id);
}
