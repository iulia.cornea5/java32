package sda.java32.service;

import sda.java32.dto.ProductCreateDTO;
import sda.java32.dto.ProductDTO;
import sda.java32.dto.SimpleProductDTO;

import java.util.List;

public interface ProductService {

    ProductDTO createProduct(ProductCreateDTO createDTO);

//    List<ProductDTO> getAllProductsForAdmin();
//
//    List<SimpleProductDTO> getAllProductsForUser();
    SimpleProductDTO get(Long id);
}
