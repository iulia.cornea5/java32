package sda.java32.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sda.java32.dto.UserCreateDTO;
import sda.java32.dto.UserDTO;
import sda.java32.entity.AppUser;
import sda.java32.repository.UserRepository;
import sda.java32.service.UserService;
import sda.java32.service.mapper.UserMapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserMapper mapper;
    private final UserRepository repo;
    private final PasswordEncoder encoder;

    public UserServiceImpl(UserMapper mapper, UserRepository repo, PasswordEncoder encoder) {
        this.mapper = mapper;
        this.repo = repo;
        this.encoder = encoder;
    }


    @Override
    public UserDTO create(UserCreateDTO createDTO) {
        AppUser toBeSaved = mapper.toEntity(createDTO);
        toBeSaved.setPassword(encoder.encode(toBeSaved.getPassword()));
        AppUser created = repo.save(toBeSaved);
        return mapper.toDTO(created);
    }

    @Override
    public List<UserDTO> getAll() {
        List<AppUser> users = repo.findAll();

        List<UserDTO> userDTOS =
                users
                        .stream()
                        .map(u -> mapper.toDTO(u))
                        .collect(Collectors.toList());

        return userDTOS;
    }

    @Override
    public UserDTO getById(Long id) {
        return mapper.toDTO(repo.findById(id).get());
    }


}
