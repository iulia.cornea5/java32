package sda.java32.service.impl;

import org.springframework.stereotype.Service;
import sda.java32.dto.ProductCreateDTO;
import sda.java32.dto.ProductDTO;
import sda.java32.dto.SimpleProductDTO;
import sda.java32.entity.Product;
import sda.java32.repository.ProductRepository;
import sda.java32.service.ProductService;
import sda.java32.service.mapper.ProductMapper;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    public ProductServiceImpl(ProductRepository repository, ProductMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public ProductDTO createProduct(ProductCreateDTO createDTO) {
        Product p = mapper.mapToEntity(createDTO);
        Product savedProduct = repository.save(p);
        ProductDTO dto = mapper.toDto(savedProduct);
        return dto;
    }



    @Override
    public SimpleProductDTO get(Long id) {
        return null;
    }
}
