package sda.java32.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.java32.dto.UserCreateDTO;
import sda.java32.dto.UserDTO;
import sda.java32.service.UserService;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {

    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping("/signup")
    public ResponseEntity<UserDTO> create(@RequestBody UserCreateDTO createDTO) {
        UserDTO created = service.create(createDTO);
        return ResponseEntity.ok(created);
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll() throws FileNotFoundException {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(service.getById(id));
    }
}
