package sda.java32.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/info")
public class InfoController {

    @GetMapping
    public ResponseEntity<String> getAppInfo() {
        return ResponseEntity.ok("Welcome! This is our JAva 32 app");
    }
}
