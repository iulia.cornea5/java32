package sda.java32.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sda.java32.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
