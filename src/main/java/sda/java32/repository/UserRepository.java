package sda.java32.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sda.java32.entity.AppUser;

public interface UserRepository extends JpaRepository<AppUser, Long> {

    AppUser findByUserName(String username);
}
