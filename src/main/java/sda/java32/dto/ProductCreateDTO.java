package sda.java32.dto;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;

@Data
@Builder
public class ProductCreateDTO {


    String name;

    String description;

    Float acquisitionPrice;

    Float sellingPrice;
}
