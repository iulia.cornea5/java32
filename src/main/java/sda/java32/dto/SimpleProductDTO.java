package sda.java32.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SimpleProductDTO {

    Long id;

    String name;

    String description;

    Float sellingPrice;
}
