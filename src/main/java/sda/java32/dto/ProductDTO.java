package sda.java32.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDTO {
    Long id;

    String name;

    String description;

    Float acquisitionPrice;

    Float sellingPrice;
}
