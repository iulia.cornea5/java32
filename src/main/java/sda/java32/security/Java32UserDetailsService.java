package sda.java32.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sda.java32.entity.AppUser;
import sda.java32.repository.UserRepository;

@Service
public class Java32UserDetailsService implements UserDetailsService {

    private final PasswordEncoder encoder;
    private final UserRepository userRepository;

    public Java32UserDetailsService(PasswordEncoder encoder, UserRepository userRepository) {
        this.encoder = encoder;
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = userRepository.findByUserName(username);
        Java32UserDetails userDetails = new Java32UserDetails(appUser);
        return userDetails;
    }
}
